import React from "react";
import { Wrapper } from "./matches.style";
import Navbar from '../Navbar/Navbar'
import EditMatchesPanel from "../../components/EditMatchesPanel/EditMatchesPanel";
import MatchesList from "../MatchesList/MatchesList";

const MatchesPage = () => {
  return (
    <Wrapper>
      <Navbar />
      <EditMatchesPanel />
      <MatchesList />
    </Wrapper>
  );
};

export default MatchesPage;
