import React from "react";
import { Wrap, StyledLink, Links } from "./navbar.style";
import firebase from "firebase";

const Navbar = () => {
  const user = firebase.auth().currentUser;

  const logOut = () => {
    if(user !== null) {
      firebase.auth().signOut();
    }
  };

  const pages = [
    {
      title: "Головна",
      key: "1",
      path: "/home",
    },
    {
      title: "Матчі",
      key: "2",
      path: "/matches",
    },
    {
      title: "Про нас",
      key: "3",
      path: "/about",
    },
  ];

  return (
    <Wrap>
      <Links>
        {pages.map((item) => {
          return (
            <StyledLink
              to={item.path}
              key={item.key}
              activeStyle={{
                color: "#f64c72",
              }}
            >
              {item.title}
            </StyledLink>
          );
        })}
      </Links>
      <Links>
      {user !== null ? 
        <StyledLink onClick={logOut} to="/login">
          Вийти, {user.displayName}
        </StyledLink> : <StyledLink to="/login">
          Увійти
        </StyledLink>
}
      </Links>
    </Wrap>
  );
};

export default Navbar;
