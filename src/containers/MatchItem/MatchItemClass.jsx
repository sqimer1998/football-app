import React from "react";
import { Wrapper, Sidebar, Overlay, CloseSidebar } from "./matchItem.style";

class MatchItemClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebar: false,
      sidebarValue: "",
    };
  }

  showSidebar = (e) => {
    this.setState(() => {
      return {
        sidebar: true,
        sidebarValue: e,
      };
    });
  };

  render() {
    return (
      <Wrapper>
        {this.state.sidebar && (
          <Overlay>
            <Sidebar className={this.state.sidebar ? "open" : ""}>
              <CloseSidebar
                onClick={() =>
                  this.setState(() => {
                    return {
                      sidebar: false,
                    };
                  })
                }
              >
                &times;
              </CloseSidebar>
              <h1>{this.state.sidebarValue}</h1>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
                aperiam quod soluta quos minus quia laborum alias earum
                repellendus provident, delectus explicabo facilis? Quibusdam,
                laborum facilis fuga eligendi aliquam mollitia.
              </p>

              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
                aperiam quod soluta quos minus quia laborum alias earum
                repellendus provident, delectus explicabo facilis? Quibusdam,
                laborum facilis fuga eligendi aliquam mollitia.
              </p>
            </Sidebar>
          </Overlay>
        )}
      </Wrapper>
    );
  }
}

export default MatchItemClass;
