import React, { useEffect, useRef, useState } from "react";
// import Sidebar from "../Sidebar/Sidebar";
import {
  Wrapper,
  WrappList,
  Text,
  Sidebar,
  Overlay,
  CloseSidebar,
} from "./matchItem.style";

const MatchItem = ({ teams }) => {
  const [sidebar, setSidebar] = useState(false);
  const [sidebarValue, setSidebarValue] = useState("");
  const ref = useRef(null)

  const showSidebar = (e) => {
    setSidebar(!sidebar);
    setSidebarValue(e);
    console.log(ref.current);
  };

  useEffect(() => {
    window.ourComponent = showSidebar;
  }, []);

  return (
    <Wrapper>
      {sidebar && (
        <Overlay>
          <Sidebar className={sidebar ? "open" : ""}>
            <CloseSidebar onClick={() => setSidebar(false)}>
              &times;
            </CloseSidebar>
            <h1>{sidebarValue}</h1>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
              aperiam quod soluta quos minus quia laborum alias earum
              repellendus provident, delectus explicabo facilis? Quibusdam,
              laborum facilis fuga eligendi aliquam mollitia.
            </p>

            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
              aperiam quod soluta quos minus quia laborum alias earum
              repellendus provident, delectus explicabo facilis? Quibusdam,
              laborum facilis fuga eligendi aliquam mollitia.
            </p>
          </Sidebar>
        </Overlay>
      )}

      {/* <WrappList>
        <Text value={teams.value.team1} onClick={showSidebar}>
          {teams.value.team1}
        </Text>
        -
        <Text value={teams.value.team2} onClick={showSidebar}>
          {teams.value.team2}
        </Text>
      </WrappList> */}
    </Wrapper>
  );
};

export default MatchItem;
