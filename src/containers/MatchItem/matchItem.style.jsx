import styled from "styled-components";

export const Wrapper = styled.div`
  text-align: center;
`;

export const WrappList = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.button`
  font-size: 1.2em;
  color: #000;
  text-align: center;
  margin: 5px;
  background-color: transparent;
  border: none;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;

  &:hover {
    color: #ff0000;
  }
`;

export const Sidebar = styled.div`
  width: 350px;
  height: calc(100vh - 54px);
  left: -350px;
  position: absolute;
  border-right: 1px solid #000;
  padding: 0 10px;
  transform: translateX(0);
  transition: transform 5s ease-in-out;

  &.open {
    transform: translateX(350px);
  }
`;

export const Overlay = styled.div`
  width: 100%;
  height: calc(100vh - 54px);
  top: 54px;
  background-color: rgba(206, 206, 206, 0);
  position: absolute;
`;

export const CloseSidebar = styled.div`
  display: flex;
  justify-content: flex-end;
  text-align: center;
  cursor: pointer;
  font-size: 2em;
`;