import React, { useState, useEffect } from "react";
import db from "../../db.config";
import { Wrapper } from "./matchesList.style";
import MatchItem from "../MatchItem/MatchItem";

const useData = () => {

  const [game, setGame] = useState([]);

  useEffect(() => {
    db.collection("game")
      .orderBy("id", "desc")
      .onSnapshot((querySnapshot) => {
        let arr = [];
        querySnapshot.docs.map((doc) =>
          arr.push({ id: doc.id, value: doc.data() })
        );
        setGame(arr);
      });
  }, [db]);
  return [game];
};

const MatchesList = () => {
  const [game] = useData();

  return (
    <Wrapper>
      {game &&
        game.map((item, i) => {
          return <MatchItem key={i} teams={item} />;
        })}
    </Wrapper>
  );
};

export default MatchesList;
