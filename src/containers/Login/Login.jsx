import React, { useState } from "react";
import { Wrapper } from "./login.style";
import { Redirect } from "react-router-dom";

import socialMediaAuth, { facebookProvider } from "../../auth";

const LoginPage = () => {
  const [login, setLogin] = useState(false);

  const handleClick = async (provider) => {
    await socialMediaAuth(provider);
    setLogin(true);
  };

  return (
    <Wrapper>
      {login && <Redirect to="/home" />}
      {!login && (
        <button onClick={() => handleClick(facebookProvider)}>Facebook</button>
      )}
    </Wrapper>
  );
};

export default LoginPage;
