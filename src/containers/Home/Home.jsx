import React from "react";
import { Wrapper, Text, Desc } from "./home.style";
import Navbar from '../Navbar/Navbar'

const HomePage = () => {
  return (
    <Wrapper>
      <Navbar />
      <Text>Вітаємо!!!</Text>
      <Desc>Ви завітали на сторінку зі створення футбольних матчів.</Desc>
    </Wrapper>
  );
};

export default HomePage;
