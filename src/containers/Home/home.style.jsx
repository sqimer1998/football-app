import styled from "styled-components";

export const Wrapper = styled.div`
    text-align: center;
`;

export const Text = styled.p`
    font-size: 1.2em;
    color: #000;
    text-align: center;
`

export const Desc = styled.p`
    font-size: 1em;
    color: #bbb;
    text-align: center;
`