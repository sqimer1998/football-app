import React from "react";
import { Wrapper, Text, Desc } from "./about.style";
import Navbar from '../Navbar/Navbar'

const AboutPage = () => {
  return (
    <Wrapper>
      <Navbar />
      <Text>Football App v1.0</Text>
      <Desc>Нова програма з допомогою якого можна створювати футбольні поєдинки.</Desc>
    </Wrapper>
  );
};

export default AboutPage;
