import React, { useState } from "react";
import { Wrapper } from "./editMatchesPanel.style";
import db from "../../db.config";
import firebase from "firebase";

const EditMatchesPanel = () => {
  const [team1, setTeam1] = useState("");
  const [team2, setTeam2] = useState("");

  const timestamp = Date.now().toString();

  const user = firebase.auth().currentUser;

  const addMatch = (e) => {
    e.preventDefault();

    if (!team1.trim() || !team2.trim()) {
      alert("Введіть команди!");
      return false;
    } else {
      db.collection("game")
        .doc(timestamp)
        .set({
          id: timestamp,
          team1: team1,
          team2: team2,
        })
        .catch(function (error) {
          console.error("Error writing Value: ", error);
        });

      setTeam1("");
      setTeam2("");
    }
  };

  return (
    <Wrapper>
      {user !== null && (
        <>
          <input
            type="text"
            id="team1"
            value={team1}
            onChange={(e) => setTeam1(e.target.value)}
          />
          <input
            type="text"
            id="team2"
            value={team2}
            onChange={(e) => setTeam2(e.target.value)}
          />
          <button type="button" onClick={addMatch}>
            Добавить матч
          </button>
        </>
      )}
    </Wrapper>
  );
};

export default EditMatchesPanel;
