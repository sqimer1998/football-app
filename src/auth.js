import firebase from "./firebase.config";

export const facebookProvider = new firebase.auth.FacebookAuthProvider();

const socialMediaAuth = (provider) => {
  return firebase
    .auth()
    .signInWithPopup(provider)
    .then((res) => res.user)
    .catch((err) => err);
};

export default socialMediaAuth;
