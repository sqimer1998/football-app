import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import MatchItemClass from "./containers/MatchItem/MatchItemClass";

ReactDOM.render(
  <MatchItemClass
    ref={(showSidebar) => {
      window.ourComponent = showSidebar;
    }}
  />,
  // <App/>,
  document.getElementById("root")
);
