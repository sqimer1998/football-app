import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCwWUBjbTq_1UoAtOytSS8W3m1e7wwzxJ8",
  authDomain: "football-app-1fbb9.firebaseapp.com",
  projectId: "football-app-1fbb9",
  storageBucket: "football-app-1fbb9.appspot.com",
  messagingSenderId: "1058704260227",
  appId: "1:1058704260227:web:b59ef4e6ebc86a1f9b6fb6",
  measurementId: "G-V9TC5S8VHD",
};

firebase.initializeApp(firebaseConfig);

export default firebase;
