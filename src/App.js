import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { GlobalStyle } from "./global";
import Matches from "./containers/Matches/Matches";
import Login from "./containers/Login/Login";
import AboutPage from "./containers/About/About";
import Home from "./containers/Home/Home";

const App = () => {
  return (
    <>
      <GlobalStyle />
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/about" component={AboutPage} />
          <Route path="/matches" component={Matches} />
          <Route path="/home" exact component={Home} />
        </Switch>
      </Router>
    </>
  );
};

export default App;
